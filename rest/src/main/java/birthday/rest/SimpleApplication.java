package birthday.rest;

import birthday.core.BirthdayService;
import birthday.core.BirthdayServiceInMemory;
import birthday.rest.resources.BirthdayResourceImpl;
import birthday.rest.resources.GreetingsResourceImpl;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

public class SimpleApplication extends ResourceConfig
{

    public SimpleApplication()
    {
        register(GreetingsResourceImpl.class);
        register(BirthdayResourceImpl.class);
        
        //register(new JacksonJsonProvider());
        
        register(new AbstractBinder()
        {
            @Override
            protected void configure()
            {
                bind(new BirthdayServiceInMemory()).to(BirthdayService.class);
            }
        });

    }
}
