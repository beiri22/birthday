package birthday.rest.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("birthdays")
public interface BirthdayResource
{

    @GET
    @Path("{name}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    Response getBirthdayInformation(@PathParam("name") String name);

    @POST
    Response addSuggestion(@Context UriInfo uriInfo, @FormParam("name") String name, @FormParam("day") int dayOfMonth, @FormParam("month") int month, @FormParam("hobby") String hobbies);

}
