package birthday.rest.resources;

import javax.ws.rs.core.Response;

import birthday.rest.resources.GreetingsResource;

public class GreetingsResourceImpl implements GreetingsResource {

	
	@Override
	public Response sayHello(String name) {
		
		HelloInfo helloInfo = new HelloInfo(name);
		
		Response r = Response.ok(helloInfo).build();
		return r;
	}
	
}
