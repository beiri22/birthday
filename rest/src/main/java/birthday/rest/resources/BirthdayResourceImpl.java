package birthday.rest.resources;

import birthday.core.BirthDayInfo;
import birthday.core.BirthdayService;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.*;

import java.util.Arrays;
import javax.inject.Inject;
import javax.ws.rs.core.Response.Status;

public class BirthdayResourceImpl implements BirthdayResource
{

    @Inject
    private BirthdayService birthdayService;

    @Override
    public Response getBirthdayInformation(String name)
    {

        if (!birthdayService.hasSuggestionsFor(name))
        {
            return Response.status(Status.NOT_FOUND).build();
        }

        BirthDayInfo suggestionsFor = birthdayService.getSuggestionsFor(name);

        return Response.ok(suggestionsFor).build();
    }

    @Override
    public Response addSuggestion(UriInfo uriInfo, String name, int dayOfMonth, int month, String hobbies)
    {
        List<String> splittedHobbies = new ArrayList<>();
        if (hobbies != null)
        {
            splittedHobbies.addAll(Arrays.asList(hobbies.split(",")));
        }

        birthdayService.addSuggestion(name, dayOfMonth, month, splittedHobbies);

        UriBuilder absolutePathBuilder = uriInfo.getAbsolutePathBuilder();
        URI created = absolutePathBuilder.path(BirthdayResource.class, "getBirthdayInformation").build(name);

        return Response.created(created).build();
    }

}
