/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.htwm.vsp.services.client;

import birthday.core.BirthDayInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.codehaus.jackson.map.ObjectMapper;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author mbenndor
 */
public class Helper {
     // -------------------------------------------------------------------------------
	// 								helper functions
	// -------------------------------------------------------------------------------
	
	/**
	 * Example code for serializing a BirthdayInfo via JAXB.
	 */
	public static void marshalBirthDayInfoToXML(BirthDayInfo b, Writer out) throws JAXBException {
		
		JAXBContext jc = JAXBContext.newInstance(BirthDayInfo.class);
		assertNotNull(jc);
		Marshaller marshaller = jc.createMarshaller();
		assertNotNull(marshaller);
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
		marshaller.marshal(b, out);
	}

	
	/**
	 * Example code for serializing a BirthdayInfo via jackson.
	 */
	public static void marshalBirthDayInfoToJSON(BirthDayInfo b, Writer out) throws IOException {
		 ObjectMapper mapper = new ObjectMapper();
		 mapper.writeValue(out, b);
	}
	
	
	/**
	 * Example code for deserializing a BirthDayInfo via JAXB.
	 */
	public static BirthDayInfo unmarshalBirthDayInfoFromXML(Reader in) throws JAXBException {
		
		JAXBContext jc = JAXBContext.newInstance(BirthDayInfo.class);
		assertNotNull(jc);
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		assertNotNull(unmarshaller);
		Object unmarshalledObj = unmarshaller.unmarshal(in);
		assertNotNull(unmarshalledObj);
		assertTrue(unmarshalledObj instanceof BirthDayInfo);
		return (BirthDayInfo) unmarshalledObj;
	}	
    
    /**
     * Method to Create a StringBulder-Object from a Buffered Reader
     */
     public static StringBuilder createStringBuilder(BufferedReader reader) throws IOException {
        StringBuilder s = new StringBuilder();
        String line = reader.readLine();
        while (line != null) {
            s.append(line);
            s.append('\n');
            line = reader.readLine();
        }
        return s;
    }
	

	/**
	 * Example code for deserializing a BirthDayInfo via jackson.
	 */
	public static BirthDayInfo unmarshalBirthDayInfoFromJSON(Reader in) throws IOException{
		 ObjectMapper mapper = new ObjectMapper();
		 return mapper.readValue(in, BirthDayInfo.class);
	}	
	
}
