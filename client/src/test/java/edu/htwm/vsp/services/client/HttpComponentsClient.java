package edu.htwm.vsp.services.client;

import static org.junit.Assert.assertEquals;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import javax.naming.NamingException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import birthday.core.BirthDayInfo;


/**
 * Shows how Apache HttpClient can be used to build a simple http REST client. 
 * 
 * @author hol
 */
public class HttpComponentsClient extends RESTClient {
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@Before
	public void setUp() throws NamingException {
	}
	
	private void verifyGreetingsForName(String name, int expectedStatusCode) throws IOException {

		System.out.println("\nrequest greetings for name " + name + " ...");
		
		HttpClient client = new HttpClient();
		
		// -- build HTTP GET request
		HttpMethod method = new GetMethod(getServiceBaseURI() + "/greetings/" + name);
		System.out.println("HTTP get resource path: " + method.getPath());
		
		int responseCode = client.executeMethod(method);
		
		// -- verify that the service responds with the expected HTTP status code  
		System.out.println("HTTP response code: " + responseCode);
		assertEquals(expectedStatusCode, responseCode);	

		if( expectedStatusCode !=  HttpURLConnection.HTTP_OK) {
			return;
		}

		// -- read http response body
		BufferedReader reader = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
		System.out.println("HTTP response body:");
        StringBuilder s = Helper.createStringBuilder(reader);
		System.out.println("\t" + s.toString());
	}
	
	
	private BirthDayInfo verifyBirthDayInfoForName(String name, int expectedStatusCode, String contentType) throws Exception {

		System.out.println("\nrequest birthday info for name " + name + " ...");
		
		HttpClient client = new HttpClient();
		
		// -- build HTTP GET request
		HttpMethod method = new GetMethod(getServiceBaseURI() + "/birthdays/" + name);
		System.out.println("HTTP get resource path: " + method.getPath());
		
		// -- determine the mime type you wish to receive here 
		method.setRequestHeader("Accept", contentType);

		int responseCode = client.executeMethod(method);
		
		// -- verify that the service responds with the expected HTTP status code  
		System.out.println("HTTP response code: " + responseCode);
		assertEquals(expectedStatusCode, responseCode);	

		if( expectedStatusCode !=  HttpURLConnection.HTTP_OK) {
			return null;
		}
		
		System.out.println("response Content-type: " + method.getResponseHeader("Content-Type").getValue());
		
		// -- read http response body
		BufferedReader reader = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
		System.out.println("HTTP response body:");
        StringBuilder s = Helper.createStringBuilder(reader);
		System.out.println("\t" + s.toString());
		
		BirthDayInfo b;
		
		if(  method.getResponseHeader("Content-Type").getValue().equals("application/json") ) {
			b = Helper.unmarshalBirthDayInfoFromJSON(new StringReader(s.toString()));
		}
		else {
			b = Helper.unmarshalBirthDayInfoFromXML(new StringReader(s.toString()));
		}
		return b;
	}
	
	// -------------------------------------------------------------------------------
	// 								test cases
	// -------------------------------------------------------------------------------
	
	
	@Test
	public void requestGreetingsTest() throws Exception {
		verifyGreetingsForName("Heinz", HttpURLConnection.HTTP_OK);
	}
	
	@Test
	public void requestBirthDayInfoTest() throws Exception {
		String name;
		BirthDayInfo result;
		
//		name = "Anne";
//		result = verifyBirthDayInfoForName(name, HttpURLConnection.HTTP_OK, "application/json");
//		System.out.println("Birthday info for " + name + ": " + result);
		
		name = "Peter";
		result = verifyBirthDayInfoForName(name, HttpURLConnection.HTTP_OK, "application/xml");
		System.out.println("Birthday info for " + name + ": " + result);

		name = "Peter";
		result = verifyBirthDayInfoForName(name, HttpURLConnection.HTTP_NOT_ACCEPTABLE, "bla");

		name = "Klaus";
		result = verifyBirthDayInfoForName(name, HttpURLConnection.HTTP_NOT_FOUND, "application/xml");
	}
	
}
